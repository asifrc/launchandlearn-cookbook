#
# Cookbook Name:: launchandlearn-cookbook
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.



include_recipe "launchandlearn-cookbook::my_recipe"
include_recipe "launchandlearn-cookbook::node"

# include_recipe 'postgresql::client'
# include_recipe 'postgresql::server'

# postgresql_database 'mr_softie' do
#   connection(
#     :host      => '127.0.0.1',
#     :port      => 5432,
#     :username  => 'postgres',
#     :password  => node['postgresql']['password']['postgres']
#   )
#   action :create
# end